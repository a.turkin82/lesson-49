import React from 'react';
import './Numbers.css';

const Numbers = (props) => {
  return (
    <div className="card-number">
      <div>
        {props.num}
      </div>
    </div>
  );
};

export default Numbers;
