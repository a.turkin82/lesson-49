import React from 'react';
import './Person.css';

const Person = (props) => {
  return (
    <>
      <div className="card-person">
        <div className="card-person__title">
          {props.title}
        </div>
        <div className={'card-person__age' + (props.age > 30 ? ' card-person__age--old' : '')}>
          {props.age}
        </div>
      </div>
    </>
  )
};

export default Person;
