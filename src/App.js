import React, { Component } from 'react';
import Person from "./components/Person";
import Numbers from "./components/Numbers";
import './App.css';

import './sass/style.sass';

class App extends Component {
  state = {
    persons: [
      { title: 'Vasiliy', age: 35 },
      { title: 'Alexey', age: 28 },
      { title: 'Irina', age: 17 },
      { title: 'Mariya', age: 25 },
      { title: 'Vlad', age: 65 },
      { title: 'Nikolay', age: 45 }
    ],
    numbers: []
  };

  onIncreaseAgeHandler = (event) => {
    event.preventDefault();
    const persons = this.state.persons.map(el => {
      return { ...el, age: el.age + 1 };
    });
    this.setState({persons});
  }

  onDecreaseAgeHandler = (event) => {
    event.preventDefault();
    const persons = this.state.persons.map(el => {
      return { ...el, age: (el.age > 0) ? el.age - 1 : 0 };
    });
    this.setState({persons});
  }

  generateNumbers = (event) => {
    event.preventDefault();
    let numbers = [];

    while (numbers.length < 5) {
      const num = Math.ceil(Math.random() * 36);
      if (num !== 0 && !numbers.includes(num)) {
        numbers.push(num);
      }
    }

    numbers = numbers.sort(function(a, b) {
      return a - b;
    });
    this.setState({numbers});
  }

  render() {

    return (
      <>
        <div className="persons-list">
          <Person title={this.state.persons[0].title} age={this.state.persons[0].age}/>
          <Person title={this.state.persons[1].title} age={this.state.persons[1].age}/>
          <Person title={this.state.persons[2].title} age={this.state.persons[2].age}/>
          <Person title={this.state.persons[3].title} age={this.state.persons[3].age}/>
          <Person title={this.state.persons[4].title} age={this.state.persons[4].age}/>
          <Person title={this.state.persons[5].title} age={this.state.persons[5].age}/>
        </div>
        <div className="persons-list">
          {this.state.numbers.map((el, idx) => {
            return (
              <Numbers key={idx} num={el}/>
            )
          })}
        </div>
        <div className="buttons-group">
          <button onClick={this.onIncreaseAgeHandler}>Increase age</button>
          <button onClick={this.onDecreaseAgeHandler}>Decrease age</button>
          <button onClick={this.generateNumbers}>Generate numbers</button>
        </div>
        <div className="test">
          <p>1</p>
          <p>2</p>
          <p>3</p>
        </div>
      </>
    )
  }
}

export default App;
